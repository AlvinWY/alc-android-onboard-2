package com.example.on_board2.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.on_board2.R;
import com.example.on_board2.entity.Account;
import com.example.on_board2.fragments.AccountDetailFragment;
import com.example.on_board2.fragments.LoginFragment;

public class AccountDetailActivity extends AppCompatActivity {

    public static final String EXTRA_LONG_PROFILE_URL = "EXTRA_LONG_MY_PROFILE_URL";
    public static final String EXTRA_LONG_FIRST_NAME = "EXTRA_LONG_MY_FIRST_NAME";
    public static final String EXTRA_LONG_LAST_NAME = "EXTRA_LONG_MY_LAST_NAME";
    public static final String EXTRA_LONG_EMAIL = "EXTRA_LONG_MY_EMAIL";


    public static final int RESULT_CONTENT_MODIFIED = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_detail);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(LoginFragment.TAG) == null) {
            // Init fragment.
            Intent extra = getIntent();
            String profileUrl = "", firstName = "", lastName = "", email = "";

            if (extra != null) {
                profileUrl = extra.getStringExtra(EXTRA_LONG_PROFILE_URL);
                firstName = extra.getStringExtra(EXTRA_LONG_FIRST_NAME);
                lastName = extra.getStringExtra(EXTRA_LONG_LAST_NAME);
                email = extra.getStringExtra(EXTRA_LONG_EMAIL);
            }
            Account account = new Account(firstName, lastName, email, profileUrl);
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, AccountDetailFragment.newInstance(account), AccountDetailFragment.TAG)
                    .commit();
        }
    }
}