package com.example.on_board2.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.on_board2.R;
import com.example.on_board2.fragments.MainFragment;
import com.example.on_board2.utils.SharedPreferenceHelper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (!SharedPreferenceHelper.getInstance(this).contains("token")) {
            // User is not login yet.
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        } else {
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, MainFragment.newInstance(), MainFragment.TAG)
                    .commit();
        }
    }
}