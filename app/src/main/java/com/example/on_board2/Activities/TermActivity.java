package com.example.on_board2.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.on_board2.R;
import com.example.on_board2.fragments.LoginFragment;
import com.example.on_board2.fragments.TermFragment;

public class TermActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(LoginFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, TermFragment.newInstance(), TermFragment.TAG)
                    .commit();
        }
    }
}