package com.example.on_board2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.on_board2.R;
import com.example.on_board2.entity.Account;

import java.util.List;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.ViewHolder> {

    private Context mContext;
    private List<Account> mData;
    private Callbacks mCallbacks;

    public AccountAdapter(Context mContext, List<Account> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_account, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout root;

        TextView title;
        TextView email;

        ImageView profilePic;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            root = itemView.findViewById(R.id.linearlayout_root);
            title = itemView.findViewById(R.id.txt_Name);
            email = itemView.findViewById(R.id.txt_Email);
            profilePic = itemView.findViewById(R.id.img_profile);
        }

        public void bindTo(Account data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                String fullTitle = data.getFirst_name()
                        + " "
                        + data.getLast_name();
                this.title.setText(fullTitle);
                this.email.setText(data.getEmail());

                Glide.with(mContext)
                        .load(data.getProfile_pic())
                        .into(this.profilePic);

                if (callbacks != null) {

                    root.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });
                }
            }
        }


        public void resetViews() {
            title.setText("");
            email.setText("");
            Glide.with(mContext)
                    .load(R.drawable.ic_baseline_account_box_90dp)
                    .into(profilePic);
            root.setOnClickListener(null);
        }
    }

    public interface Callbacks {
        void onListItemClicked(Account data);
    }
}
