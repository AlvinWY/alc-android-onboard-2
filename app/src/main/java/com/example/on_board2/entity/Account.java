package com.example.on_board2.entity;

public class Account {
    String first_name;
    String last_name;
    String email;
    String profile_pic;

    public Account(String first_name, String last_name, String email, String profile_pic) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.profile_pic = profile_pic;
    }

    public Account() {
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
