package com.example.on_board2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.on_board2.R;
import com.example.on_board2.entity.Account;


public class AccountDetailFragment extends Fragment {

    public static final String TAG = AccountDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_PROFILE_URL = "ARG_LONG_MY_PROFILE_URL";
    private static final String ARG_LONG_FIRST_NAME = "ARG_LONG_MY_FIRST_NAME";
    private static final String ARG_LONG_LAST_NAME = "ARG_LONG_MY_LAST_NAME";
    private static final String ARG_LONG_EMAIL = "ARG_LONG_MY_EMAIL";

    protected String profile_Url = null, first_Name, last_Name, email;

    protected TextView mTxtFirstName;
    protected TextView mTxtLastName;
    protected TextView mTxtEmail;
    protected ImageView mImgProile;

    public static Fragment newInstance(Account account) {
        Bundle args = new Bundle();
        args.putString(ARG_LONG_PROFILE_URL, account.getProfile_pic());
        args.putString(ARG_LONG_FIRST_NAME, account.getFirst_name());
        args.putString(ARG_LONG_LAST_NAME, account.getLast_name());
        args.putString(ARG_LONG_EMAIL, account.getEmail());

        AccountDetailFragment fragment = new AccountDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_detail, container, false);
        mTxtFirstName = (TextView) view.findViewById(R.id.txt_first_name_detail);
        mTxtLastName = (TextView) view.findViewById(R.id.txt_last_name_detail);
        mTxtEmail = (TextView) view.findViewById(R.id.txt_email_detail);
        mImgProile = (ImageView) view.findViewById(R.id.img_profile_detail);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            profile_Url = args.getString(ARG_LONG_PROFILE_URL);
            first_Name = args.getString(ARG_LONG_FIRST_NAME);
            last_Name = args.getString(ARG_LONG_LAST_NAME);
            email = args.getString(ARG_LONG_EMAIL);
        }

        initView();
    }

    private void initView() {
        if (profile_Url != null) {
            mTxtFirstName.append("\n" + first_Name);
            mTxtLastName.append("\n" + last_Name);
            mTxtEmail.append("\n" + email);
            Glide.with(getView())
                    .load(profile_Url)
                    .into(mImgProile);
        }
    }
}