package com.example.on_board2.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.on_board2.R;
import com.example.on_board2.activities.MainActivity;
import com.example.on_board2.activities.TermActivity;
import com.example.on_board2.utils.SharedPreferenceHelper;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import gsonmodels.LoginModel;


public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();

    public static final String reqresAPI = "https://reqres.in/api/";

    Button btn_Login;
    EditText txt_Email, txt_Password;
    TextView txt_Term;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        btn_Login = view.findViewById(R.id.btn_login);
        txt_Email = (EditText) view.findViewById(R.id.txt_email);
        txt_Password = (EditText) view.findViewById(R.id.txt_password);
        txt_Term = (TextView) view.findViewById(R.id.txt_terms);
        txt_Email.setText("eve.holt@reqres.in");
        txt_Password.setText("cityslicka");
        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    doLogin(v);
                } catch (Exception e) {
                    Log.d("Lifecycle", String.valueOf(e));
                }
            }
        });

        txt_Term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), TermActivity.class));
            }
        });

        return view;
    }

    protected void doLogin(View v) {
        btn_Login = v.findViewById(R.id.btn_login);

        String email = txt_Email.getText().toString().trim();
        String password = txt_Password.getText().toString();
        btn_Login.setEnabled(false);
        btn_Login.setText("Loading");

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setMessage("Please enter credentials")
                    .setTitle("Notice")
                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            btn_Login.setEnabled(true);
                            btn_Login.setText("Login");
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            //Call API
            String url = reqresAPI + "login";
            RequestQueue queue = Volley.newRequestQueue(getActivity());

            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    // Convert JSON string to Java object.
                    LoginModel responseModel = new GsonBuilder().create().fromJson(response, LoginModel.class);
                    // Save user's email to shared preference.
                    SharedPreferenceHelper.getInstance(getActivity())
                            .edit()
                            .putString("email", email)
                            .putString("token", responseModel.token)
                            .apply();

                    btn_Login.setEnabled(true);
                    btn_Login.setText("Login");

                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finish();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Invalid email or password")
                            .setTitle("Authentication Failed")
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    btn_Login.setEnabled(true);
                                    btn_Login.setText("Login");
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("email", email);
                    params.put("password", password);

                    return params;
                }
            };

            queue.add(stringRequest);
        }
    }

}