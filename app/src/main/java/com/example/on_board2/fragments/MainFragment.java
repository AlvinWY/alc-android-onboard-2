package com.example.on_board2.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.on_board2.R;
import com.example.on_board2.activities.AccountDetailActivity;
import com.example.on_board2.activities.LoginActivity;
import com.example.on_board2.adapter.AccountAdapter;
import com.example.on_board2.entity.Account;
import com.example.on_board2.utils.SharedPreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainFragment extends Fragment implements AccountAdapter.Callbacks {

    public static final String TAG = MainFragment.class.getSimpleName();

    private static String Json_Url = "https://reqres.in/api/users?page=2";
    private final int REQUEST_CODE_MY_NOTE_DETAIL = 300;

    protected List<Account> mAccountList;
    private AccountAdapter mAdapter;

    protected RecyclerView mRecyclerView;

    public static Fragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mAccountList = new ArrayList<>();
        mAdapter = new AccountAdapter(getContext(), mAccountList);
        mAdapter.setCallbacks(this);

        GetData getData = new GetData();
        getData.execute();
    }

    @Override
    public void onListItemClicked(Account data) {
        Intent intent = new Intent(getActivity(), AccountDetailActivity.class);

        intent.putExtra(AccountDetailActivity.EXTRA_LONG_PROFILE_URL, data.getProfile_pic());
        intent.putExtra(AccountDetailActivity.EXTRA_LONG_FIRST_NAME, data.getFirst_name());
        intent.putExtra(AccountDetailActivity.EXTRA_LONG_LAST_NAME, data.getLast_name());
        intent.putExtra(AccountDetailActivity.EXTRA_LONG_EMAIL, data.getEmail());
        startActivityForResult(intent, REQUEST_CODE_MY_NOTE_DETAIL);
    }

    public class GetData extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String current = "";
            try {
                URL url;
                HttpURLConnection urlConnection = null;

                try {
                    url = new URL(Json_Url);
                    urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream inputStreams = urlConnection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStreams);

                    int data = inputStreamReader.read();
                    while (data != -1) {
                        current += (char) data;
                        data = inputStreamReader.read();
                    }
                    return current;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return current;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    Account accountModel = new Account();
                    accountModel.setFirst_name(jsonObject1.getString("first_name"));
                    accountModel.setLast_name(jsonObject1.getString("last_name"));
                    accountModel.setEmail(jsonObject1.getString("email"));
                    accountModel.setProfile_pic(jsonObject1.getString("avatar"));

                    mAccountList.add(accountModel);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            PutDataIntoRecyclerView(mAccountList);
        }
    }

    private void PutDataIntoRecyclerView(List<Account> mAccountList) {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mRecyclerView = view.findViewById(R.id.recyclerView);


        return view;
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.btn_Logout) {
            // TODO show confirm dialog before continue.
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Are you sure?")
                    .setTitle("Log Out")
                    .setCancelable(true)
                    .setPositiveButton("No",null)
                    .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Logout user.
                            // Clear all user's data.
                            SharedPreferenceHelper.getInstance(getActivity())
                                    .edit()
                                    .clear()
                                    .apply();

                            // Go to login page.
                            startActivity(new Intent(getActivity(), LoginActivity.class));
                            getActivity().finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}