package com.example.on_board2.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.on_board2.R;

public class TermFragment extends Fragment {

    public static final String TAG = TermFragment.class.getSimpleName();

    private static final String url_Term = "https://generator.lorem-ipsum.info/terms-and-conditions";

    private WebView view_term;

    public static Fragment newInstance() {
        return new TermFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_term, container, false);
        view_term = (WebView) view.findViewById(R.id.view_term);
        view_term.setWebViewClient(new myWebViewClient());
        view_term.loadUrl(url_Term);
        return view;
    }
    public class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading (WebView view, String url) {
            if (Uri.parse(url).getHost().equals("www.google.com")) {
                return false;
            }
            return true;
        }
    }

}