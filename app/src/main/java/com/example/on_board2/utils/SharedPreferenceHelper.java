package com.example.on_board2.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceHelper {

    private static SharedPreferences mInstance;

    public static SharedPreferences getInstance(Context context) {
        if (mInstance == null) {
            synchronized (com.example.on_board2.utils.SharedPreferenceHelper.class) {
                if (mInstance == null) {
                    mInstance = context.getSharedPreferences("app", Context.MODE_PRIVATE);
                }
            }
        }

        return mInstance;
    }

    private SharedPreferenceHelper() {
    }
}
